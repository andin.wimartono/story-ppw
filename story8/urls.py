from django.urls import path
from .views import *

app_name = 'story8'

urlpatterns = [
    path('', index, name='index'),
    path('data/', data, name='books_data')
]
